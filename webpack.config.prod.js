const webpack = require('webpack');
const JsDocPlugin = require('jsdoc-webpack-plugin');

module.exports = {
  entry: './js/main.js',
  output: {
    path: './dist/',
    filename: 'app.min.js'
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }]
  },
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: true }
    }),
    new JsDocPlugin({
      conf: './jsdoc.conf'
    })
  ]
};
