class AST_Node
{
  constructor(data, opCount, precedence, operands = null) {
    this.data       = data;
    this.opCount    = opCount;
    this.precedence = precedence;
    this.operands   = operands;
  }

  expression_string() {
    let l_str = "";
    if (this.left instanceof AST_Node)
      l_str = this.left.expression_string() + " ";
    else if (this.left)
      l_str = this.left + " ";

    let r_str = "";
    if (this.right instanceof AST_Node)
      r_str = " " + this.right.expression_string();
    else if (this.right)
      r_str = " " + this.right;
    
    return l_str + this.data + r_str;
  }
}

class Parser
{
  constructor() {
    this.tokens = new Map();
  }

  registerToken(token, metadata) {
    this.tokens.set(token, metadata);
  }

  popParams(n, stack) {
    let params = new Array(n);
    for (let i = n - 1; i >= 0; i--) {
      params[i] = stack.pop();
    }

    return params;
  }

  pushOp(op, op_stack, stack) {
    while (op_stack.length != 0 &&
           op.precedence <= op_stack[op_stack.length-1].precedence) {
      let params = this.popParams(op.opCount, stack);
      stack.push(new AST_Node(op_stack.pop(), op.opCount,
                              op.precedence, params));
    }

    op_stack.push(op);
  }

  precedencePass(tokens) {
    let stack = [],
        op_stack = [];
    tokens.map(tok_str => {
      if (this.tokens.has(tok_str)) {
        let op = this.tokens.get(tok_str);

        this.pushOp(op, op_stack, stack);
      }
      else {
        stack.push(new AST_Node(tok_str, 0, 1));
      }
    });

    return [stack, op_stack];
  }

  lastPass(tokens, stack, op_stack) {
    while (op_stack.length != 0) {
      let token = op_stack.pop();

      let params = this.popParams(token.opCount, stack);

      stack.push(new AST_Node(token, token.opCount, token.precedence, params));
    }

    // If there is no ops left there will always be at least one value left
    // inside the stack. This is guarenteed since a string of size zero
    // will not be parsed.
    return stack.pop();
  }

  buildAst(tokens) {
    if (tokens.length == 0) {
      console.log("buildAst was called with no tokens");
      return null;
    }
    let [stack, op_stack] = this.precedencePass(tokens);
    return this.lastPass(tokens, stack, op_stack);
  }
}
