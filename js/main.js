let cur_table = null;

function create_table_header(header) {
  let tr = document.createElement("TR");
  header.map(function(x) {
    let th = document.createElement("TH");
    let text = document.createTextNode(x);

    th.appendChild(text);
    tr.appendChild(th);
  });

  return tr;
}

function create_table_contents(table, contents) {
  contents.map(function(arr) {
    let tr = document.createElement("TR");

    arr.map(function(x) {
      let td = document.createElement("TD");
      let text = document.createTextNode(x);

      td.appendChild(text);
      tr.appendChild(td);
    });

    table.appendChild(tr);
  });
}

/**
 * Creates a DOM table. The table consists of a row header
 * followed by a 2d array of contents.
 * @param {number[]} header The table header
 * @param {number[][]} contents The values of the table
 */
function create_table(header, contents) {
  let tbl = document.createElement("TABLE");
  tbl.id = "ttable";

  tbl.appendChild(create_table_header(header));
  create_table_contents(tbl, contents);

  document.body.appendChild(tbl);
}

/**
 * Extracts from num the bit at position pos
 * @param {number} num The number whose bit will be extracted
 * @param {number} pos The bit position
 * @returns {number} Either 0 or 1
 */
function nth_bit(num, pos) {
  return (num >> pos) & 0x1;
}

/**
 * Takes a one dimensional array and assigns the first 
 * variable.length positions the cartesian product of selecting
 * true of false, that is, it generates all permutations of assigning
 * true or false for each row of the truth table.
 */
function fill_variable_columns(table, variables) {
  for (let r = 0; r < table.length; r++) {
    table[r] = new Array(variables.length);
    for (let c = 0; c < table[r].length; c++) {
      table[r][table[r].length - c - 1] = new Boolean(nth_bit(r, c));
    }
  }
  return table;
}

function fill_negation_columns(ast, table) {

}

function fill_expression_columns(ast, table) {
  
}

function eval_expression(str) {
  let tokens = str.split(" ");

  /* Extract variables in the expression */
  // regex for testing alpha characters excluding v
  let rgx = /[a-uw-zA-Z]$/;
  let letters = tokens.filter(x => rgx.test(x));
  letters = [...new Set(letters)];
  letters.sort();

  /* Preallocate */
  let contents = new Array(Math.pow(2, letters.length));

  let ast = buildAst(tokens);

  fill_variable_columns(letters, contents);
  fill_negation_columns(ast, contents);
  fill_expression_columns(ast, contents);

  return arr;
}

function create_truth_table() {
  /* get bool expression */
  let expr = document.getElementsByTag("input").text;

  /* eval bool expression */
  let header_contents = eval_expression(expr);

  if (cur_table !== null) {
    let tbl = document.getElementById("ttable");
    tbl.parentNode.removeChild(tbl);
  }

  /* set table */
  cur_table = header_contents;

  /* create dom truth table */
  let header = header_contents[0];
  let contents = header_contents[1];
  create_table(header, contents);
}

function main() {
  let num = 0;
  console.log(num);
  let btn = document.getElementsByTagName("button")[0];
  btn.onclick = function() {
    create_table(["p", "q", "p ^ q"],
                 [[0,   0,   0], 
                  [0,   1,   0], 
                  [1,   0,   0], 
                  [1,   1,   1]]);
  };
}

window.onload = main;
